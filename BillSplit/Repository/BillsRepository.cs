﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using BillSplit.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository
{
    public class BillsRepository : ApplicationRepository<Bill>, IBillsRepository
    {
        public BillsRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public override Bill GetSingle(Guid id)
        {
            return _context.Bills.Where(b => b.Id == id)
                .Include(b => b.Payments)
                .Include(b => b.DebtorGroups)
                .Include(b => b.User)
                .FirstOrDefault();
        }

        public List<Bill> Get(Expression<Func<Bill, bool>> where = null, int? limit = null, Func<IQueryable<Bill>,
          IQueryable<Bill>> order = null)
        {
            var Bills = base.Get(where, limit, order)
                .Include(b => b.DebtorGroups)
                .ToList();

            return Bills;

        }

        public List<Bill> GetBillsAndDebtorsWithPayments(DateTime startDate, DateTime endDate, Expression<Func<Bill, bool>> where = null, int? limit = null, Func<IQueryable<Bill>,
      IQueryable<Bill>> order = null)
        {
            var Bills = base.Get(where, limit, order)
                .Include(b => b.Payments.Where(p => p.Date >= startDate && p.Date <= endDate))
                .Include(b => b.DebtorGroups)
                .Include(b => b.DebtorGroups.Select(dg => dg.DebtorPayments.Where(p => p.Date >= startDate && p.Date <= endDate)))
                .Include(b => b.DebtorGroups.Select(dg => dg.Debtor))
                .ToList();

            return Bills;

        }

    }
}
