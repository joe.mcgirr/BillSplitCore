﻿using BillSplit.Models;
using BillSplit.Models.AccountModels;
using BillSplit.Models.Interfaces;
using BillSplit.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace BillSplit.Repository.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        protected readonly DbContextOptions _options;
        IHttpContextAccessor _httpContextAccessor;
        //Custom sets
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillPayment> BillPayments { get; set; }
        public DbSet<DebtorGroupInstance> DebtorGroups { get; set; }
        public DbSet<DebtorPayment> DebtorPayments { get; set; }
        public DbSet<Debtor> Debtors { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IHttpContextAccessor httpContextAccessor)
            : base(options)
        {
            _options = options;
            _httpContextAccessor = httpContextAccessor; 
        }

        public override int SaveChanges()
        {
            //not a seeding scenario
            if (_httpContextAccessor.HttpContext != null)
            {
                AddIdAndTimeStamps();
            }
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            //not a seeding scenario
            if (_httpContextAccessor.HttpContext != null)
            {
                AddIdAndTimeStamps();
            }
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddIdAndTimeStamps()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IModel && (x.State == EntityState.Added || x.State == EntityState.Modified));

            var identityName =_httpContextAccessor.HttpContext.User.FindFirst("email")?.Value ?? "Anonymous";
            var now = DateTime.UtcNow;

            foreach (var change in modifiedEntries)
            {
                if (change.State == EntityState.Added)
                {
                    ((IModel)change.Entity).Created = now;
                    ((IModel)change.Entity).UserCreated = identityName;
                }
                ((IModel)change.Entity).Updated = now;
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //rename aspnetusers to apllicationUser
            builder.Entity<ApplicationUser>()
                        .ToTable("ApplicationUsers");
            //set delete cascade settings
        }
    }
}
