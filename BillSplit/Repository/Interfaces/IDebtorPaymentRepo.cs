﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IDebtorPaymentRepo
    {
        DebtorPayment GetSingle(Guid id);
        List<DebtorPayment> Get(Expression<Func<DebtorPayment, bool>> where = null, int? limit = null, Func<IQueryable<DebtorPayment>,
          IQueryable<DebtorPayment>> order = null);
    }
}
