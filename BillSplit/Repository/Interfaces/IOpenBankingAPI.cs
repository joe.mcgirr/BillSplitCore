﻿using BillSplit.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IOpenBankingAPI
    {
        Task<bool> GetBillPaymentsForUserAsync(DateTime startDate, Guid currentUserId);
        Task GetDebtorPaymentsForUserAsync(DateTime startDate, Guid currentUserId);


    }
}
