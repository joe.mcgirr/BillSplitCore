﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IBillPaymentRepo : IApplicationRepository<BillPayment>
    {
        BillPayment GetSingle(Guid id);
        List<BillPayment> Get(Expression<Func<BillPayment, bool>> where = null, int? limit = null, Func<IQueryable<BillPayment>,
            IQueryable<BillPayment>> order = null);
    }
}
