﻿using BillSplit.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IApplicationRepository<T> where T : class, IModel
    {
        T GetSingle(Guid id);
        IQueryable<T> Get(Expression<Func<T, bool>> where = null, int? limit = null,
            Func<IQueryable<T>, IQueryable<T>> order = null);
        void Add(T entity);
        void Update(T entity);
        void SaveChanges();
    }
}
