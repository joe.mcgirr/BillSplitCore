﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IUserRepository : IApplicationRepository<ApplicationUser>
    {

    }
}
