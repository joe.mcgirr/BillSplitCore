﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IDebtorRepository : IApplicationRepository<Debtor>
    {
        Debtor GetSingle(Guid id);
        List<Debtor> Get(Expression<Func<Debtor, bool>> where = null, int? limit = null, Func<IQueryable<Debtor>,
          IQueryable<Debtor>> order = null);
    }
}
