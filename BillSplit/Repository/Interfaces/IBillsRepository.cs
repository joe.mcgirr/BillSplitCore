﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IBillsRepository : IApplicationRepository<Bill> 
    {
        Bill GetSingle(Guid id);
        List<Bill> Get(Expression<Func<Bill, bool>> where = null, int? limit = null, Func<IQueryable<Bill>,
          IQueryable<Bill>> order = null);
        List<Bill> GetBillsAndDebtorsWithPayments(DateTime startDate, DateTime endDate, Expression<Func<Bill, bool>> where = null, int? limit = null, Func<IQueryable<Bill>,
      IQueryable<Bill>> order = null);
    }
}
