﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public interface IDebtorGroupRepo : IApplicationRepository<DebtorGroupInstance> 
    {
        DebtorGroupInstance GetSingle(Guid id);

        List<DebtorGroupInstance> Get(Expression<Func<DebtorGroupInstance, bool>> where = null, int? limit = null, Func<IQueryable<DebtorGroupInstance>,
          IQueryable<DebtorGroupInstance>> order = null);

        List<DebtorGroupInstance> GetDebtorGroupsWithPaymentsWithinDates(DateTime startDate, DateTime endDate,
            Expression<Func<DebtorGroupInstance, bool>> where = null, int? limit = null, Func<IQueryable<DebtorGroupInstance>,
         IQueryable<DebtorGroupInstance>> order = null);
        }
    
}
