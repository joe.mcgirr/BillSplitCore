﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository.Interfaces
{
    public class BillPaymentRepo : ApplicationRepository<BillPayment>, IBillPaymentRepo
    {
        public BillPaymentRepo(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public BillPayment GetSingle(Guid id)
        {
            return _context.BillPayments.Where(b => b.Id == id)
                .FirstOrDefault();
        }

        public List<BillPayment> Get(Expression<Func<BillPayment, bool>> where = null, int? limit = null, Func<IQueryable<BillPayment>,
            IQueryable<BillPayment>> order = null)
        {
            var BillPayments = base.Get(where, limit, order)
                .Include(p => p.Bill)
                .ToList();

            return BillPayments;
        }
    }
}
