﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace BillSplit.Repository.Interfaces
{
    public class DebtorRepository : ApplicationRepository<Debtor>, IDebtorRepository
    {
        public DebtorRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public override Debtor GetSingle(Guid id)
        {
            return _context.Debtors.Where(dg => dg.Id == id)
                .FirstOrDefault();
        }

        public List<Debtor> Get(Expression<Func<Debtor, bool>> where = null, int? limit = null, Func<IQueryable<Debtor>,
          IQueryable<Debtor>> order = null)
        {
            var Debtors = base.Get(where, limit, order)
                .ToList();

            return Debtors;

        }
    }
}
