﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Repository
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            context.Database.EnsureCreated();
            //initialize identity users
            if (context.Users.Any())
            {
                return; //already seeded
            }

            var user1Id = Guid.NewGuid();
            var user2Id = Guid.NewGuid();
            var user1 = new ApplicationUser { Id = user1Id, UserName = "joe.mcgirr@hotmail.co.uk", Email = "joe.mcgirr@hotmail.co.uk", FirstName = "Joe", LastName = "McGirr" };
            var user2 = new ApplicationUser { Id = user2Id, UserName = "joe.mcgirr2@hotmail.co.uk", Email = "joe.mcgirr2@hotmail.co.uk", FirstName = "Jane", LastName = "Doe" };

            var result = userManager.CreateAsync(user1, "gpwd_2015!?").Result;
            var result2 = userManager.CreateAsync(user2, "gpwd_2015!?").Result;

            if(!result.Succeeded || !result.Succeeded)
            {
                throw new Exception("Failed to add user:" + result.ToString());
            }
            //initialize bills
            var bills = new Bill[]
            {
                new Bill{Id = Guid.NewGuid(), UserId = user1Id, Name = "Energy", Cost = 80.50m, StatementReference = "503053236001", Recurrence = Recurrence.monthly,
                         FirstPayment = DateTime.Now.AddMonths(-4), LastPayment = null, LinkedToBank = true, Created = DateTime.Now, UserCreated = "System" },
                new Bill{Id = Guid.NewGuid(), UserId = user1Id, Name = "Water", Cost = 20.00m, StatementReference = "48300838182018", Recurrence = Recurrence.weekly,
                        FirstPayment = DateTime.Now.AddMonths(-2), LastPayment = DateTime.Now.AddMonths(4), LinkedToBank = true, Created = DateTime.Now, UserCreated = "System" }

            };
            foreach (Bill b in bills)
            {
                context.Bills.Add(b);
            }
            context.SaveChanges();
            //initialize debtors
            var debtors = new Debtor[]
            {
                new Debtor{Id = Guid.NewGuid(), UserId = user1Id, Name = "Nuala Flynn", PhoneNumber = "0743834674394", Created = DateTime.Now, UserCreated = "System"},
                new Debtor{Id = Guid.NewGuid(), UserId = user1Id, Name = "George Spitorious", PhoneNumber = "0743834674394", Created = DateTime.Now, UserCreated = "System"},
                new Debtor{Id = Guid.NewGuid(), UserId = user1Id, Name = "Katie Botteril", PhoneNumber = "4934943943493", Created = DateTime.Now, UserCreated = "System"},
            };
            //Initialize debtor groups
            var debtorGroups = new DebtorGroupInstance[]
            {
                new DebtorGroupInstance{Id = Guid.NewGuid(), BillId = bills[0].Id, DebtorId = debtors[0].Id, AddedToBill = DateTime.Now, Reminders = true,
                    DebtReference = "XMRXY4S", AmountOwed = 15.00m, Created = DateTime.Now, UserCreated = "System"},
                new DebtorGroupInstance{Id = Guid.NewGuid(), BillId = bills[0].Id, DebtorId = debtors[1].Id, AddedToBill = DateTime.Now, Reminders = true,
                    DebtReference = "YXWDYES", AmountOwed = 15.00m, Created = DateTime.Now, UserCreated = "System"},
                new DebtorGroupInstance{Id = Guid.NewGuid(), BillId = bills[1].Id, DebtorId = debtors[0].Id, AddedToBill = DateTime.Now, Reminders = true,
                    DebtReference = "2YDJ2JS", AmountOwed = 5.00m, Created = DateTime.Now, UserCreated = "System"}
            };

            foreach (Debtor d in debtors)
            {
                context.Debtors.Add(d);
            }

            foreach (DebtorGroupInstance dg in debtorGroups)
            {
                context.DebtorGroups.Add(dg);
            }

            context.SaveChanges();

        }
    }
}
