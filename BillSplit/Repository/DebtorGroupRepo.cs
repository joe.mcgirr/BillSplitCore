﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using BillSplit.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository
{
    public class DebtorGroupRepo : ApplicationRepository<DebtorGroupInstance>, IDebtorGroupRepo
    {
        public DebtorGroupRepo(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public DebtorGroupInstance GetSingle(Guid id)
        {
            return _context.DebtorGroups.Where(dg => dg.Id == id)
                .FirstOrDefault();
        }


        public List<DebtorGroupInstance> Get(Expression<Func<DebtorGroupInstance, bool>> where = null, int? limit = null, Func<IQueryable<DebtorGroupInstance>,
          IQueryable<DebtorGroupInstance>> order = null)
        {
            var Bills = base.Get(where, limit, order)
                .Include(dg => dg.Bill)
                .Include(dg => dg.DebtorPayments)
                .Include(dg => dg.Debtor)
                .ToList();

            return Bills;

        }

        public List<DebtorGroupInstance> GetDebtorGroupsWithPaymentsWithinDates(DateTime startDate, DateTime endDate,
            Expression<Func<DebtorGroupInstance, bool>> where = null, int? limit = null, Func<IQueryable<DebtorGroupInstance>,
         IQueryable<DebtorGroupInstance>> order = null)
        {
            var Bills = base.Get(where, limit, order)
                .Include(dg => dg.Bill)
                .Include(dg => dg.DebtorPayments.Where(p => p.Date >= startDate && p.Date <= endDate))
                .Include(dg => dg.Debtor)
                .ToList();

            return Bills;

        }
    }
}
