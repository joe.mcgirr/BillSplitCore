﻿using BillSplit.Models;
using BillSplit.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using BillSplit.Repository.Contexts;
using Microsoft.EntityFrameworkCore;

namespace BillSplit.Repository
{
    public class DebtorPaymentRepo : ApplicationRepository<DebtorPayment>, IDebtorPaymentRepo
    {
        public DebtorPaymentRepo(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public DebtorPayment GetSingle(Guid id)
        {
            return _context.DebtorPayments.Where(dp => dp.Id == id)
                .FirstOrDefault();
        }

        public List<DebtorPayment> Get(Expression<Func<DebtorPayment, bool>> where = null, int? limit = null, Func<IQueryable<DebtorPayment>,
          IQueryable<DebtorPayment>> order = null)
        {
            var Bills = base.Get(where, limit, order)
                .Include(b => b.DebtorGroupInstance.Bill)
                .ToList();

            return Bills;

        }
    }
}
