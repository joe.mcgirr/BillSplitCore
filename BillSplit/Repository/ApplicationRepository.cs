﻿using BillSplit.Models.Interfaces;
using BillSplit.Repository.Contexts;
using BillSplit.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BillSplit.Repository
{
    public class ApplicationRepository<T> : IApplicationRepository<T> where T : class, IModel
    {
        protected ApplicationDbContext _context;

        public ApplicationRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public virtual T GetSingle(Guid id)
        {
            return _context.Set<T>().SingleOrDefault(m => m.Id == id);
        }

        public virtual IQueryable<T> Get(Expression<Func<T, bool>> where = null, int? limit = null,
            Func<IQueryable<T>, IQueryable<T>> order = null)
        {
            IQueryable<T> query = _context.Set<T>();

            if (where != null)
            {
                query = query.Where(where);
            }

            if (order != null)
            {
                query = order(query);
            }

            if (limit.HasValue)
            {
                query = query.Take(limit.Value);
            }


            return query;

        }

        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);

        }

        public void AddOrUpdate(T entity)
        {

            _context.Entry(entity).State = entity.Id == default(Guid) ? EntityState.Added : EntityState.Modified;

            _context.SaveChanges();

        }

        public void Update(T entity)
        {

            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;

        }

        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
