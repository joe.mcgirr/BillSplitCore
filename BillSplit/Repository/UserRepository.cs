﻿using BillSplit.Models;
using BillSplit.Repository.Contexts;
using BillSplit.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Repository
{
    public class UserRepository : ApplicationRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
