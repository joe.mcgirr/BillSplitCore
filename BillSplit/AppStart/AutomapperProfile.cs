﻿using AutoMapper;
using BillSplit.Models;
using BillSplit.Models.RequestModels;
using BillSplit.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.AppStart
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<AddBillDTO, Bill>();
            CreateMap<AddDebtorToBillDTO, Debtor>();
            CreateMap<DebtorPayment, DebtorPaymentDTO>();
            CreateMap<BillPayment, BillPaymentDTO>();
            CreateMap<AddNewDebtorDTO, Debtor>();
        }
    }
}
