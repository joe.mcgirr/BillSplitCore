﻿using BillSplit.Infrastructure;
using BillSplit.Repository;
using BillSplit.Repository.Interfaces;
using BillSplit.Services;
using BillSplit.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BillSplit
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
        #region Services
            services.AddScoped<IBillsService, BillsService>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<ITimeService, TimeService>();
            services.AddScoped<ITellerService, TellerService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();        

        #endregion
        #region Repositories
        services.AddScoped<IBillsRepository, BillsRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDebtorRepository, DebtorRepository>();
            services.AddScoped<IDebtorGroupRepo, DebtorGroupRepo>();
            services.AddScoped<IBillPaymentRepo, BillPaymentRepo>();
            services.AddScoped<IDebtorPaymentRepo, DebtorPaymentRepo>();

        #endregion

            //register delegating handlers
            services.AddTransient<HttpClientAuthorizationDelegatingHandler>();

            services.AddHttpClient<IMessagingService, MessagingService>(
                client => { client.DefaultRequestHeaders.Add("Accept", "application/json");})
                .SetHandlerLifetime(TimeSpan.FromMinutes(5))
                //Adds authorization header to each request 
                .AddHttpMessageHandler<HttpClientAuthorizationDelegatingHandler>()
                //Perform exponential backoff on failure
                .AddPolicyHandler(GetRetryPolicy())
                .AddPolicyHandler(GetCircuitBreakerPolicy());
            return services;
        }

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
              .HandleTransientHttpError()
              .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
              .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

        }
        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
        }
    }
}
