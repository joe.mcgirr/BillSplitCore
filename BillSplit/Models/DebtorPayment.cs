﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public class DebtorPayment : Model
    {
        public DebtorGroupInstance DebtorGroupInstance { get; set; }
        //is a required relationship
        public Guid DebtorGroupInstanceId { get; set; }
        public decimal AmountPayed { get; set; }
        public DateTime Date { get; set; }
        public string PaymentReference { get; set; }
        //helps differentiate payments that are identical in ref, ammount and date but happened in a different teller read
        public Guid TellerAccessId { get; set; }
    }
}
