﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.RequestModels
{
    public class AddDebtorToBillDTO
    {
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public bool Reminders { get; set; }
        public Guid BillId { get; set; }
        public decimal AmountOwed { get; set; }
    }
}
