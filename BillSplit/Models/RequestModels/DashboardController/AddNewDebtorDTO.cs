﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.RequestModels
{
    public class AddNewDebtorDTO
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
