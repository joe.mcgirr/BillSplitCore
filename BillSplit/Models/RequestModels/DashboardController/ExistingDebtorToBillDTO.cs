﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.RequestModels.DashboardController
{
    public class ExistingDebtorToBillDTO
    {
        public Guid DebtorId { get; set; }
        public Guid BillId { get; set; }
        public decimal ToPay { get; set; }
        public bool Reminders { get; set; }
    }
}
