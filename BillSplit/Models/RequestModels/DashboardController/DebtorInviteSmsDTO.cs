﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.RequestModels.DashboardController
{
    public class DebtorInviteSmsDTO
    {
        public string BillOwnerName { get; set; }
        public string BillName { get; set; }
        public string DebtorName { get; set; }
        public decimal BillCost { get; set; }
        public string DebtReference { get; set; }
        public string PhoneNumber { get; set; }
    }
}
