﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.RequestModels
{
    public class AddBillDTO
    {
        public string Name { get; set; }
        public bool LinkedToBank { get; set; }
        [Required]
        public decimal Cost { get; set; }
        [Required]
        public string StatementName { get; set; }
        public string StatementReference { get; set; }
        public Recurrence Recurrence { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
