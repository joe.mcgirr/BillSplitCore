﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public class DebtorGroupInstance : Model
    {
        public Bill Bill { get; set; }
        [ForeignKey("Bill")]
        public Guid BillId { get; set; }
        public Debtor Debtor { get; set; }
        [ForeignKey("Debtor")]
        public Guid? DebtorId { get; set; }
        public DateTime AddedToBill { get; set; }
        //TODO: Future improvement-set the date/time for a reminder
        public bool Reminders { get; set; }
        //used to ID payments to this bill from this debtor
        public string DebtReference { get; set; }
        //amount of the bill they will pay
        public decimal AmountOwed { get; set; }

        public ICollection<DebtorPayment> DebtorPayments { get; set; }

    }
}
