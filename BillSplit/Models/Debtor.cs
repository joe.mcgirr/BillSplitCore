﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public class Debtor : Model
    {
        public string Name { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }   
        //possibly use this for generic payments so they don't need new reference every time they make one-off payments
        public string PhoneNumber { get; set; }
        public ICollection<DebtorGroupInstance> DebtorGroups { get; set; }
    }
}
