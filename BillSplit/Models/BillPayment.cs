﻿using System;

namespace BillSplit.Models
{
    public class BillPayment : Model
    {
        public Bill Bill { get; set; }
        public Guid BillId { get; set; }
        public decimal AmountPayed { get; set; }
        public DateTime Date { get; set; }
        public string Reference { get; set; }
        //helps differentiate payments that are identical in ref, ammount and date but happened in a different teller read
        public Guid TellerAccessId { get; set; }
    }
}
