﻿using Microsoft.AspNetCore.Identity;
using System;

namespace BillSplit.Models.AccountModels
{
    public class ApplicationRole : IdentityRole<Guid>
    {
    }
}
