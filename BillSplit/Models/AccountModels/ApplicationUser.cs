﻿using BillSplit.Models.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public class ApplicationUser : IdentityUser<Guid>, IModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
      
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string UserCreated { get; set; }

        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<Debtor> Debtors { get; set; }
    }
}
