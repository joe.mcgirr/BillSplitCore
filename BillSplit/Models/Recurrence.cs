﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public enum Recurrence
    {
        oneOff,
        daily,
        weekly,
        monthly,
        yearly
    }
}
