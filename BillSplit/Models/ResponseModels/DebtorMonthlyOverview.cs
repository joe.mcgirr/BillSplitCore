﻿using System;
using System.Collections.Generic;

namespace BillSplit.Models.ResponseModels
{
    public class DebtorMonthlyOverview
    {
        public DebtorMonthlyOverview(string name, Guid id)
        {
            DebtorName = name;
            DebtorId = id;
        }
        public string DebtorName { get; set; }
        public decimal TotalOwed { get; set; }
        public decimal TotalPaid { get; set; }
        public Guid DebtorId { get; set; }
        public List<BillsMonthlyOverview> BillsAndPayments { get; set; }
    }
}