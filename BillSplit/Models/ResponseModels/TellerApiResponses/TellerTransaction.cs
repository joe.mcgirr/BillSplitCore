﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillSplit.Models.Interfaces;

namespace BillSplit.Models.ResponseModels.TellerApiResponses
{
    public class TellerTransaction : IBankTransaction
    {
        public string Type { get; set; }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string Counterparty { get; set; }
        public decimal Amount { get; set; }
        public string Reference { get; set; }

    }
}
