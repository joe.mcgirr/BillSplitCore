﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.ResponseModels
{
    public class DebtorPaymentDTO
    {
        public decimal AmountPayed { get; set; }
        public DateTime Date { get; set; }
        public string PaymentReference { get; set; }
    }
}
