﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.ResponseModels
{
    public class BillPaymentDTO
    {
        public decimal AmountPayed { get; set; }
        public DateTime Date { get; set; }
    }
}
