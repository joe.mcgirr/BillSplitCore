﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.ResponseModels
{
    public class MonthlyOverview
    {
        public MonthlyOverview(DateTime date)
        {
            Date = date;
        }
        public DateTime Date { get; set; }
        public List<DebtorMonthlyOverview> DebtorMonthlyOverviews { get; set; }


    }
}
