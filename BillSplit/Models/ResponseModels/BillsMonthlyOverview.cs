﻿using System;
using System.Collections.Generic;

namespace BillSplit.Models.ResponseModels
{
    public class BillsMonthlyOverview
    {
        public string BillName { get; set; }
        public Guid BillId { get; set; }
        public decimal BillCost { get; set; }
        public decimal TotalPaid { get; set; }
        public List<BillPaymentDTO> BillPayments { get; set;}
        public List<DebtorPaymentDTO> DebtorsPayments { get; set; }
    }
}