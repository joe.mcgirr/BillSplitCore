﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models.Interfaces
{
    public interface IBankTransaction
    {
        string Type { get; set; }
        Guid Id { get; set; }
        string Description { get; set; }
        DateTime Date { get; set; }
        string Counterparty { get; set; }
        decimal Amount { get; set; }
        string Reference { get; set; }
    }
}
