﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Models
{
    public class Bill : Model
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public string StatementReference { get; set; }
        public Recurrence Recurrence { get; set; }
        public DateTime FirstPayment { get; set; }
        public DateTime? _lastPayment;
        public DateTime? LastPayment
        {
            get
            {
                //Ensures if payment is a one-off, the lastPayment date is set identical to FirstPayment
                return Recurrence == Recurrence.oneOff ? FirstPayment : _lastPayment;
            }
            set
            {
                this._lastPayment = value;
            }
        }

        public decimal GetMonthlyCost(DateTime lastDayOfCurrentMonth)
        {
            //this is done already in billService, but do again to make sure, in case used elsewhere in code 
            var daysInMonth = (DateTime.DaysInMonth(lastDayOfCurrentMonth.Year, lastDayOfCurrentMonth.Month));
            var daysBetweenLastPaymentAndEndOfMonth = 0;

           
            if (LastPayment != null && lastDayOfCurrentMonth >= LastPayment.Value && lastDayOfCurrentMonth.Month == LastPayment.Value.Month)
            {
                daysBetweenLastPaymentAndEndOfMonth = daysInMonth - LastPayment.Value.Day;
            }

            switch (Recurrence)
            {
                case Recurrence.daily:
                    return Cost * (daysInMonth - daysBetweenLastPaymentAndEndOfMonth);
                case Recurrence.weekly:
                    var weeksBetweenLastPaymentAndEndOfMonth = Convert.ToInt64(Math.Floor(Convert.ToDouble(daysBetweenLastPaymentAndEndOfMonth / 7)));
                    return Cost * (4 - weeksBetweenLastPaymentAndEndOfMonth);
                default:
                    return Cost;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="monthDifferenceBetweenDates">Must take into account years difference (in months) as well, not just months</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public bool IsPaymentDueForThisDateRange(int monthDifferenceBetweenDates, DateTime startDate, DateTime endDate)
        {
            switch (Recurrence)
            {
                //Daily and monthly bills will have payments due for any date range as date range will always be 1 or more months
                case Recurrence.oneOff:
                case Recurrence.daily:
                case Recurrence.weekly:
                case Recurrence.monthly:
                    //has bill begun yet?
                    if (FirstPayment.Date <= endDate.Date)
                    {
                        var billEndedBeforeStartTime = LastPayment == null || LastPayment.Value.Date >= startDate.Date;
                        //has the bill ended before start of time frame
                        return billEndedBeforeStartTime;
                    }
                    return false;
                    //TODO: Yearly will work as long as start and end date remain 6 months apart, otherwise needs rewritting
                case Recurrence.yearly:
                    //has bill begun yet?
                    if (FirstPayment.Date <= endDate.Date)
                    {
                        //bill hasn't ended before time frame
                        if (LastPayment == null || LastPayment.Value.Date >= startDate.Date)
                        {
                            //adds the actual int values i.e doesn't modulo at 12 after adding like .addMonths does
                            var startDateMonthIntVal = startDate.Month;
                            var endDateMonthIntVal = startDate.Month + monthDifferenceBetweenDates;
                            //if date range spans over 2 years, work out firstpayment months added to a years worth of months...
                            var firstPaymentMonthIntVal = endDateMonthIntVal > 12 ? 12 + FirstPayment.Month : FirstPayment.Month;

                            if (startDateMonthIntVal <= firstPaymentMonthIntVal && firstPaymentMonthIntVal < endDateMonthIntVal)
                            {
                                return true;
                            }

                        }
                    }
                    return false;
                default:
                    return false;

            }      
        }

        //Are bill payments monitored through banking API?
        public bool LinkedToBank { get; set; }

        public ICollection<BillPayment> Payments { get; set; }
        public ICollection<DebtorGroupInstance> DebtorGroups { get; set; }
    }
}
