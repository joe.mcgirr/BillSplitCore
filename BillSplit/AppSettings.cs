﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit
{
    public class AppSettings
    {
        public string MessagingUrl { get; set; }

        public class Connectionstrings
        {
            public string DefaultConnection { get; set; }
        }

        public string JwtIssuer { get; set; }
        public int JwtExpireDays { get; set; }
        public string JwtKey { get; set; }


        public class Logging
        {
            public bool IncludeScopes { get; set; }
            public Loglevel LogLevel { get; set; }
        }

        public class Loglevel
        {
            public string Default { get; set; }
        }
    }
}
