﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Infrastructure
{
    public static class API
    {
        public static class Messaging
        {
            //this class allows you to add HTTP endpoints, query strings/parameters etc to URI
            public static string AddSmsToQueue(string baseUri)
            {
                return $"{baseUri}AddSmsToQueue";
            }
        }
    }
}
