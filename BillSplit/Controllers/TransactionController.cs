﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BillSplit.Services.Interfaces;
using BillSplit.Models.ResponseModels.TellerApiResponses;

namespace BillSplit.Controllers
{
    [Produces("application/json")]
    [Route("api/Transaction")]
    public class TransactionController : BaseController
    {
        ITellerService _tellerService;
        public TransactionController(ITellerService tellerService)
        {
            _tellerService = tellerService;
        }

        [Authorize]
        [HttpGet("RecentTransactions")]
        public async Task<IActionResult> UpdateRecentTransactionsAsync()
        {
            await _tellerService.GetBillPaymentsForUserAsync(DateTime.Now.AddMonths(-5), CurrentUserId);
            await _tellerService.GetDebtorPaymentsForUserAsync(DateTime.Now.AddMonths(-5), CurrentUserId);
            return Ok(new { Success = true });
        }

        [Authorize]
        [HttpGet("AllBankTransactions")]
        public async Task<IActionResult> ListTransactions()
        {
            List<TellerTransaction> transactions = await _tellerService.GetTransactionsAsync("3910e187-064a-4279-958e-5cd72b931700");
            return Ok(new { transactions = transactions });
        }
    }
}