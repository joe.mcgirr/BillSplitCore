﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net;
using BillSplit.Models;

namespace BillSplit.Controllers
{
    [Produces("application/json")]
    [Route("api/Base")]
    public class BaseController : Controller
    {
        private ApplicationUser _currentUser;
        private Guid _currentUserId;

        public virtual Guid CurrentUserId
        {
            get
            {
                if (_currentUserId != default(Guid))
                    return _currentUserId;

                if (User == null)
                {
                    return Guid.Empty;
                }
                _currentUserId = Guid.Parse(User.FindFirst("sub")?.Value);
                return _currentUserId;
            }
        }

        //public virtual ApplicationUser CurrentUser
        //{
        //    get
        //    {
        //        if (_currentUser != null) return _currentUser;

        //        if (CurrentUserId == Guid.Empty)
        //        {
        //            return null;
        //        }

        //        _currentUser = _services.UserService.GetById(CurrentUserId);

        //        return _currentUser;
        //    }
        //    set { _currentUser = value; }
        //}

        //protected HttpResponseMessage Success(dynamic content)
        //{
        //    return Response(HttpStatusCode.OK, "", content);
        //}

        /// <summary>
        /// Throws and error from the API with http status code and body content as json
        /// </summary>
        /// <param name="code"></param>
        /// <param name="errorMessage"></param>
        /// <param name="bodyContent"></param>
        /// <returns></returns>
        protected new HttpResponseMessage Response(string statusCode, dynamic reasonPhrase)
        {
            Enum.TryParse<HttpStatusCode>(statusCode, out HttpStatusCode statusCodeEnum);
            return new HttpResponseMessage()
            {
                ReasonPhrase = reasonPhrase,
                StatusCode = statusCodeEnum,
            };          
        }

    }
}
