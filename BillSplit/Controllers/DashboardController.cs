﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BillSplit.Services.Interfaces;
using AutoMapper;
using BillSplit.Models.RequestModels;
using BillSplit.Models;
using Microsoft.EntityFrameworkCore;
using BillSplit.Models.RequestModels.DashboardController;

namespace BillSplit.Controllers
{
    [Produces("application/json")]
    [Route("api/Dashboard")]
    public class DashboardController : BaseController
    {
        IBillsService _billService;
        IMapper _mapper;
        IMessagingService _messagingService;
        public DashboardController(IBillsService billService, IMapper mapper, IMessagingService messagingService
)
        {
            _billService = billService;
            _messagingService = messagingService;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet("DashboardData")]
        IActionResult DashboardData()
        {
            try
            {
                var bills = _billService.GetBillOverviews(DateTime.Now.AddMonths(-5), DateTime.Now.AddMonths(1), CurrentUserId);
                return Ok(bills);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPost("NewBill")]
        public IActionResult NewBill([FromBody]List<AddBillDTO> billModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _billService.AddNewBillToUser(billModel, CurrentUserId);
                    return Ok( new 
                    {
                        Success = true
                    });
                }

                return BadRequest(new
                {
                    ModelState = ModelState
                });
            }
            catch (DbUpdateException e)
            {
                return BadRequest( new
                {
                    Message = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost("NewDebtor")]
        public IActionResult NewDebtor([FromBody]List<AddNewDebtorDTO> debtorModels)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _billService.AddNewDebtor(debtorModels, CurrentUserId);
                    return Ok(new
                    {
                        Success = true
                    });
                }
                return BadRequest(new
                {
                    ModelState = ModelState
                });
            }
            catch (DbUpdateException e)
            {
                return BadRequest(new
                {
                    Message = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost("NewDebtorToBill")]
        public IActionResult NewDebtorToBill([FromBody]AddDebtorToBillDTO debtorModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var smsMessageDetails = _billService.AddNewDebtorToBill(debtorModel, CurrentUserId);
                    _messagingService.SendInviteSmsMessageAsync(smsMessageDetails, CurrentUserId);
                    return Ok(new
                    {
                        Success = true
                    });
                }
                return BadRequest(new
                {
                    ModelState = ModelState
                });
            }
            catch (DbUpdateException e)
            {
                return BadRequest(new
                {
                    Message = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost("ExistingDebtorToBill")]
        public IActionResult ExistingDebtorToBill([FromBody] ExistingDebtorToBillDTO model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var smsMessageDetails = _billService.AddExistingDebtorToBill(model.DebtorId, model.BillId, model.Reminders, model.ToPay, CurrentUserId);
                    _messagingService.SendInviteSmsMessageAsync(smsMessageDetails, CurrentUserId);

                    return Ok(new
                    {
                        Success = true
                    });
                }
                return BadRequest(new
                {
                    ModelState = ModelState
                });
            }
            catch (DbUpdateException e)
            {
                return BadRequest(new
                {
                    Message = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost("IsBillProviderUnique")]
        public IActionResult BillProviderUnique(string billReference)
        {
            return Ok(new { Success = true });
        }

        [Authorize]
        [HttpPost("BillReferenceUnique")]
        public IActionResult BillReferenceUnique(string billReference)
        {
            return Ok(new { Success = true });
        }
    }
}