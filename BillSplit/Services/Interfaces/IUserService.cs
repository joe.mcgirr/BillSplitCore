﻿using BillSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services.Interfaces
{
    public interface IUserService
    {
        string GetUserEmail();
    }
}
