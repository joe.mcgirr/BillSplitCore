﻿using BillSplit.Models.RequestModels.DashboardController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services.Interfaces
{
    public interface IMessagingService
    {
        Task SendInviteSmsMessageAsync(DebtorInviteSmsDTO messageDetails, Guid userId);
    }
}
