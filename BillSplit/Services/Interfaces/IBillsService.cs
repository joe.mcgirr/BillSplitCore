﻿using BillSplit.Models;
using BillSplit.Models.RequestModels;
using BillSplit.Models.RequestModels.DashboardController;
using BillSplit.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services.Interfaces
{
     public interface IBillsService
    {
        List<MonthlyOverview> GetBillOverviews(DateTime startDate, DateTime endDate, Guid currentUserId);
        void AddNewBillToUser(ICollection<AddBillDTO> newBill, Guid currentUserId);
        void AddNewDebtor(ICollection<AddNewDebtorDTO> debtorModels, Guid currentUserId);
        void AddExistingDebtorToBill(Guid debtorId, Guid billId, bool reminders, decimal toPay, Guid currentUserId);
        DebtorInviteSmsDTO AddNewDebtorToBill(AddDebtorToBillDTO debtorModel, Guid currentUserID);
    }
}
