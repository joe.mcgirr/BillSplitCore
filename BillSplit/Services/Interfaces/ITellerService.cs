﻿using BillSplit.Models;
using BillSplit.Models.ResponseModels.TellerApiResponses;
using BillSplit.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services.Interfaces
{
    public interface ITellerService : IOpenBankingAPI
    {
        Task<List<TellerTransaction>> GetTransactionsAsync(string accountId);
        DebtorGroupInstance DebtorGroupInstanceThatMatchesTransactionName(ICollection<DebtorGroupInstance> debtorGroups, TellerTransaction transaction);

    }
}
