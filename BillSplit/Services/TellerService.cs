﻿using BillSplit.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillSplit.Models;
using BillSplit.Models.ResponseModels.TellerApiResponses;
using System.Net.Http;
using Microsoft.IdentityModel.Protocols;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using ReadAsAsyncCore;
using System.Text.RegularExpressions;
using BillSplit.Repository.Interfaces;

namespace BillSplit.Services
{
    public class TellerService : ITellerService
    {
        static HttpClient client;
        private readonly IConfiguration _configuration;
        private readonly IBillsRepository _billRepo;
        private readonly IBillPaymentRepo _billPaymentRepo;
        private readonly IDebtorPaymentRepo _debtorPaymentRepo;
        private readonly IDebtorGroupRepo _debtorGroupRepo;
        public TellerService(IConfiguration configuration, IBillsRepository billRepo, IBillPaymentRepo billPaymentRepo, IDebtorPaymentRepo debtorPaymentRepo,
            IDebtorGroupRepo debtorGroupRepo)
        {
            _configuration = configuration;
            _billRepo = billRepo;
            _billPaymentRepo = billPaymentRepo;
            _debtorPaymentRepo = debtorPaymentRepo;
            _debtorGroupRepo = debtorGroupRepo;

            client = new HttpClient();
            InitClient();
        }
        void InitClient()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("https://api.teller.io/");
            var bearerToken = _configuration["TellerApiKey"];
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + bearerToken);
        }

        public DebtorGroupInstance DebtorGroupInstanceThatMatchesTransactionName(ICollection<DebtorGroupInstance> debtorGroups, TellerTransaction transaction)
        {
            return null;
        }

        public async Task<bool> GetBillPaymentsForUserAsync(DateTime startDate, Guid currentUserId)
        {
            var currentTellerAccessId = Guid.NewGuid();

            List<TellerTransaction> transactions = await GetTransactionsAsync("a096f7d4-a399-49d9-bcda-2df4f721ebb1");
            foreach (var transaction in transactions)
            {
                if (transaction.Date > startDate)
                {
                    AddReferenceToTransactions(transaction);
                    //TODO: Very risky method of checking the reference - change this maybe apply machine learning??
                    var pertainingBill = _billRepo.Get(b => b.UserId == currentUserId && b.LinkedToBank && transaction.Reference.Contains(b.StatementReference)).FirstOrDefault();
                    if (pertainingBill == null)
                    {
                        continue;
                    }
                    if (!BillPaymentPreviouslyReadToDb(transaction, currentTellerAccessId, currentUserId))
                    {
                        var newPayment = new BillPayment()
                        {
                            BillId = pertainingBill.Id,
                            AmountPayed = transaction.Amount,
                            Date = transaction.Date,
                            Reference = transaction.Reference,
                            TellerAccessId = currentTellerAccessId
                        };
                        _billPaymentRepo.Add(newPayment);
                    }
                }
                _billPaymentRepo.SaveChanges();
            }
            return true;
        }

        public async Task GetDebtorPaymentsForUserAsync(DateTime startDate, Guid currentUserId)
        {
            var currentTellerAccessId = Guid.NewGuid();

            List<TellerTransaction> transactions = await GetTransactionsAsync("3910e187-064a-4279-958e-5cd72b931700");
            foreach (var transaction in transactions)
            {
                if (transaction.Date > startDate)
                {
                    AddReferenceToTransactions(transaction);                  
                    var debtorGroupsWithMatchingReference = _debtorGroupRepo.Get(dg => dg.Debtor.UserId == currentUserId
                                                        && transaction.Reference.ToLower().Contains(dg.DebtReference.ToLower())).FirstOrDefault();
                   
                    if (debtorGroupsWithMatchingReference == null)
                    {
                        continue;
                    }
                    if (!DebtorPaymentPreviouslyReadToDb(transaction, currentTellerAccessId, currentUserId))
                    {
                        var newPayment = new DebtorPayment()
                        {
                            DebtorGroupInstance = debtorGroupsWithMatchingReference,
                            AmountPayed = transaction.Amount,
                            Date = transaction.Date,
                            PaymentReference = transaction.Reference,
                            TellerAccessId = currentTellerAccessId
                        };
                        debtorGroupsWithMatchingReference.DebtorPayments.Add(newPayment);
                    }
                }
            }
            _debtorGroupRepo.SaveChanges();

        }

        public async Task<List<TellerTransaction>> GetTransactionsAsync(string accountId)
        {
            //hardcoded - obviously change
            var path = @"accounts/" + accountId + "/transactions";
            List<TellerTransaction> transactions = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                transactions = await response.Content.ReadAsJsonAsync<List<TellerTransaction>>();
            }
            //handle failure here
            return transactions;
        }

        private void AddReferenceToTransactions(TellerTransaction transaction)
        {
            //tries to remove reference from extra text added by the bank
            var referenceRegex = new Regex(@"(?<=^REF\s).*(?=,)|(?<=^REFERENCE\s).*(?=,)|(?:.*REFERENCE [-]?)(.*)");
            if (referenceRegex.IsMatch(transaction.Description))
            {
                transaction.Reference = referenceRegex.Match(transaction.Description).Groups[0].Value.TrimEnd();
            }
            else
            {
                transaction.Reference = transaction.Description;
            }
        }

        //TODO: Logic for name matching
        private bool NameMatchesDebtor(string debtorGroupName, string transctionName)
        {
            if (transctionName.ToLower().Contains(debtorGroupName.ToLower()))
            {
                return true;
            }
            return false;

            // for all check out of order / check for two middle names.. /
            //check for j t mcgirr
            //var subStrings = debtorGroupName.Split(' ');
            //check for joe thomas mcgirr
            //check for joe t mcgirr
            //check for j mcgirr
            //check for REGULAR TRANSFER FROM MISS NUALA COLLEEN FLYNN REFERENCE
        }

        private bool DebtorPaymentPreviouslyReadToDb(TellerTransaction transaction, Guid currentTellerAccessId, Guid currentUserId)
        {
            //identical payment: p.paymentreference will have been assigned after adding to db 
            var paymentPreviouslyRead = _debtorPaymentRepo.Get(p => p.DebtorGroupInstance.Bill.UserId == currentUserId && p.PaymentReference == transaction.Reference && p.AmountPayed == transaction.Amount &&
                                        p.Date == transaction.Date && p.TellerAccessId != currentTellerAccessId).Any();
            if (paymentPreviouslyRead)
            {
                return true;
            }
            return false;
        }

        private bool BillPaymentPreviouslyReadToDb(TellerTransaction transaction, Guid currentTellerAccessId, Guid currentUserId)
        {
            //identical bill exists from a previous read-in - safe to assume bank transactions are immutable (only looking for new bills)
            var billPreviouslyRead = _billPaymentRepo.Get(p => p.Bill.UserId == currentUserId && p.Reference == transaction.Reference && p.AmountPayed == transaction.Amount &&
                                        p.Date == transaction.Date && p.TellerAccessId != currentTellerAccessId).ToList().Any();
            if (billPreviouslyRead)
            {
                return true;
            }
            return false;
        }
    }
}
