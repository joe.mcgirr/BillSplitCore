﻿using BillSplit.Models;
using BillSplit.Repository.Interfaces;
using BillSplit.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services
{
    public class UserService : IUserService
    {
        IHttpContextAccessor _httpContext;
        IUserRepository _userRepository;

        public UserService(IHttpContextAccessor httpContext, IUserRepository userRepo)
        {
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _userRepository = userRepo ?? throw new ArgumentNullException(nameof(userRepo));
        }

        public ApplicationUser GetUser()
        {
            var userEmail = _httpContext.HttpContext.User.Identity.Name;
            var user = _userRepository.Get(u => u.Email == userEmail).FirstOrDefault();
            return user;
        }

        public string GetUserEmail()
        {
            return _httpContext.HttpContext.User.Identity.Name;
        }
    }
}
