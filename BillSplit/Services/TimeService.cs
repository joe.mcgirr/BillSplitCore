﻿using BillSplit.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillSplit.Services
{
    public class TimeService : ITimeService
    {
        public DateTime CurrentTime => DateTime.UtcNow;
    }
}

