﻿using AutoMapper;
using BillSplit.Models;
using BillSplit.Models.RequestModels;
using BillSplit.Models.RequestModels.DashboardController;
using BillSplit.Models.ResponseModels;
using BillSplit.Repository.Interfaces;
using BillSplit.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillSplit.Services
{
    public class BillsService : IBillsService
    {
        private readonly IBillsRepository _billRepo;
        private readonly IDebtorGroupRepo _debtorGroupRepo;
        private readonly IDebtorRepository _debtorRepo;
        private IMapper _mapper;

        public BillsService(IBillsRepository billRepo, IMapper mapper, IDebtorGroupRepo debtorGroupRepo, IDebtorRepository debtorRepo)
        {
            _billRepo = billRepo;
            _mapper = mapper;
            _debtorGroupRepo = debtorGroupRepo;
            _debtorRepo = debtorRepo;
        }
     
        /// <summary>
        /// Get monthly billoverviews for each debtor (dashboard page on app). Search dates must be at least a month appart, default is 6 months. 
        /// </summary>
        /// <param name="startDate">month to search from</param>
        /// <param name="endDate">month to search untill</param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public List<MonthlyOverview> GetBillOverviews(DateTime startDate, DateTime endDate, Guid currentUserId)
        {
            if(startDate.Date > endDate.Date)
            {
                throw new ArgumentException("start date must precede end date");
            }

            //includes duration of last month as part of the difference
            var monthDifferenceBetweenDates = MonthDifferenceBetweenDates(startDate, endDate);
            if (monthDifferenceBetweenDates < 1)
            {
                throw new ArgumentException("There must be at least a months difference between start and end search dates");
            }
            var firstDayOfStartDate = new DateTime(startDate.Year, startDate.Month, 1);
            var lastDayOfEndDate = firstDayOfStartDate.AddMonths(monthDifferenceBetweenDates).AddDays(-1);
            
            //load bills payed within dates given from the db or all bills not linked to bank account
            var billsPayedWithinDates = _billRepo.GetBillsAndDebtorsWithPayments(firstDayOfStartDate, lastDayOfEndDate, b => b.UserId == currentUserId && b.IsPaymentDueForThisDateRange(monthDifferenceBetweenDates, firstDayOfStartDate, lastDayOfEndDate));

            var monthlyOverviews = new List<MonthlyOverview>();
            //for each month in date range : less than or equal to as to include the start month in monthDifference
            for (int i = 0; i < monthDifferenceBetweenDates; i++)
            {
                var currentMonthStartDay = firstDayOfStartDate.AddMonths(i);
                var currentMonthEndDay = firstDayOfStartDate.AddMonths(i + 1).AddDays(-1);
                //get bills for this month by checking the bill payments that have came out as well as the next expect payment date for future dates
                var billsForMonth = billsPayedWithinDates.Where(b => b.IsPaymentDueForThisDateRange(1, currentMonthStartDay, currentMonthEndDay)).ToList();

                //initialise monthlyoverview model - even if no bills, should have an empty month overview
                var monthlyOverview = new MonthlyOverview(currentMonthStartDay);
                monthlyOverview.DebtorMonthlyOverviews = new List<DebtorMonthlyOverview>();

                //people who should have paid user back for bills in current month
                var allDebtorGroupsForMonth = billsForMonth.SelectMany(b => b.DebtorGroups).ToList();
                var allDebtorsForMonth = allDebtorGroupsForMonth.Select(dg => dg.Debtor).Distinct().ToList();               
             
                foreach (var debtor in allDebtorsForMonth)
                {
                    var debtorOverview = new DebtorMonthlyOverview(debtor.Name, debtor.Id);

                    var debtorsDebtorGroupsForMonth = allDebtorGroupsForMonth.Where(dg => dg.DebtorId == debtor.Id).ToList();
                    var billsForDebtor = billsForMonth.Where(b => b.DebtorGroups.Any(dg => dg.DebtorId == debtor.Id));
                    debtorOverview.BillsAndPayments = billsForDebtor.Select(b => new BillsMonthlyOverview()
                    {
                        BillName = b.Name,
                        BillId = b.Id, 
                        BillCost = b.GetMonthlyCost(currentMonthEndDay),
                        TotalPaid = debtorsDebtorGroupsForMonth.Where(dg => dg.BillId == b.Id && dg.DebtorId == debtor.Id)
                                .SelectMany(dg => dg.DebtorPayments).Sum(dp => dp.AmountPayed),
                        //use mapper to map to DTO's
                        //Select the bill payments for bill within months date range
                        BillPayments = _mapper.Map<ICollection<BillPayment>, ICollection<BillPaymentDTO>>(b.Payments.Where(p => p.Date >= currentMonthStartDay && 
                        p.Date <= currentMonthEndDay).OrderBy(p => p.Date).ToList()).ToList(),
                        //select debtor payments that match debtor and current bill and are within months date range
                        DebtorsPayments = _mapper.Map<List<DebtorPayment>, List<DebtorPaymentDTO>>(
                        debtorsDebtorGroupsForMonth.Where(dg => dg.BillId == b.Id && dg.DebtorId == debtor.Id)
                                .SelectMany(dg => dg.DebtorPayments.Where(p => p.Date >= currentMonthStartDay &&
                        p.Date <= currentMonthEndDay).OrderBy(p => p.Date)).ToList())
                    }).ToList();
                    debtorOverview.TotalOwed = debtorOverview.BillsAndPayments.Sum(b => b.BillCost);
                    debtorOverview.TotalPaid = debtorOverview.BillsAndPayments.Sum(b => b.DebtorsPayments.Sum(p => p.AmountPayed));
                    monthlyOverview.DebtorMonthlyOverviews.Add(debtorOverview);
                }
                monthlyOverviews.Add(monthlyOverview);
            }

            return monthlyOverviews.ToList();
        }


        public void AddNewBillToUser(ICollection<AddBillDTO> billModels, Guid currentUserId)
        {
            foreach (var billModel in billModels)
            {
                var bill = _mapper.Map<AddBillDTO, Bill>(billModel);
                //ValidateBill(newBill); unique..
                bill.UserId = currentUserId;
                _billRepo.Add(bill);
                _billRepo.SaveChanges();
            }

        }

        public void AddNewDebtor(ICollection<AddNewDebtorDTO> debtorModels, Guid currentUserId)
        {
            foreach (var debtorModel in debtorModels)
            {
                var debtor = _mapper.Map<AddNewDebtorDTO, Debtor>(debtorModel);
                //ValidateDebtor(); -- CHECK PHONE NUMBER
                //TODO: Does this set the navigation property as well??
                debtor.UserId = currentUserId;
                _debtorRepo.Add(debtor);
                _debtorRepo.SaveChanges();
            }

        }

        public DebtorInviteSmsDTO AddNewDebtorToBill(AddDebtorToBillDTO debtorModel, Guid currentUserID)
        {
            var debtor = _mapper.Map<AddDebtorToBillDTO, Debtor>(debtorModel);
            debtor.UserId = currentUserID;
            var pertainingBill = _billRepo.GetSingle(debtorModel.BillId);
            var debtorBillReference = GenerateReferenceFromBill(pertainingBill);

            var debtorGroup = new DebtorGroupInstance()
            {
                BillId = debtorModel.BillId,
                Debtor = debtor,
                AddedToBill = DateTime.Now,
                Reminders = debtorModel.Reminders,
                AmountOwed = debtorModel.AmountOwed,
                DebtReference = debtorBillReference
            };

            pertainingBill.DebtorGroups.Add(debtorGroup);
            _debtorGroupRepo.Add(debtorGroup);
            _debtorGroupRepo.SaveChanges();

            return new DebtorInviteSmsDTO()
            {
                BillOwnerName = pertainingBill.User.FirstName,
                BillName = pertainingBill.Name,
                DebtorName = debtor.Name,
                BillCost = debtorModel.AmountOwed,
                DebtReference = debtorBillReference,
                PhoneNumber = debtor.PhoneNumber
   
            };

        }

        public DebtorInviteSmsDTO AddExistingDebtorToBill(Guid debtorId, Guid billId, bool reminders, decimal toPay, Guid currentUserId)
        {

            var pertainingBill = _billRepo.Get(b => b.Id == billId && b.UserId == currentUserId).FirstOrDefault();
            //CHECK IF THIS DEBTOR EXISTS FOR USER!
            var pertainingDebtor = _debtorRepo.Get(d => d.Id == debtorId && d.UserId == currentUserId).FirstOrDefault();
            var debtorBillReference = GenerateReferenceFromBill(pertainingBill);

            var debtorGroup = new DebtorGroupInstance()
            {
                BillId = billId,
                Debtor = pertainingDebtor,
                AddedToBill = DateTime.Now,
                Reminders = reminders,
                DebtReference = debtorBillReference,
                AmountOwed = toPay
            };

            _debtorGroupRepo.Add(debtorGroup);
            pertainingBill.DebtorGroups.Add(debtorGroup);
            _debtorGroupRepo.SaveChanges();

            return new DebtorInviteSmsDTO()
            {
                BillOwnerName = pertainingBill.User.FirstName,
                BillName = pertainingBill.Name,
                DebtorName = pertainingDebtor.Name,
                BillCost = toPay,
                DebtReference = debtorBillReference,
                PhoneNumber = pertainingDebtor.PhoneNumber
            };
        }
      
        private int MonthDifferenceBetweenDates(DateTime startDate, DateTime endDate)
        {
            var monthDifferenceBetweenYears = ((endDate.Year - startDate.Year) * 12);
            //includes duration of last month as part of the difference
            var monthDifference = endDate.AddMonths(1).Month - startDate.Month;
            return monthDifferenceBetweenYears + monthDifference;
        }

        //TODO: Decide between this or Hash of bill name and debtor number
        private string GenerateReferenceFromBill(Bill pertainingBill)
        {
            //max length of bank statement reference
            var maxLength = 14;
            var cypherlength = 6;

            char[] _base62chars =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            .ToCharArray();

            Random _random = new Random();

            var sb = new StringBuilder(cypherlength);

            for (int i = 0; i < cypherlength; i++)
                //only uppercase
                sb.Append(_base62chars[_random.Next(36)]);

            var reference = sb.ToString();
            //CheckReferenceIsUnique(reference);
            return reference;
        }
    }

}
