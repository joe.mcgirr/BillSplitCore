﻿using BillSplit.Infrastructure;
using BillSplit.Models.RequestModels.DashboardController;
using BillSplit.Services.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BillSplit.Services
{
    public class MessagingService : IMessagingService
    {
        private readonly string _remoteServiceBaseUrl;
        private readonly AppSettings _settings;
        private readonly HttpClient _httpClient;

        public MessagingService(HttpClient httpClient, IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _settings = settings.Value;
            _remoteServiceBaseUrl = $"{_settings.MessagingUrl}/api/sms/";
        }
        public async Task SendInviteSmsMessageAsync(DebtorInviteSmsDTO messageDetails, Guid userId)
        {
            var uri = API.Messaging.AddSmsToQueue(_remoteServiceBaseUrl);
            var message = CreateInviteMessage(messageDetails);
            var messageObject = new SmsMessageResponse()
            {
                PhoneNumber = messageDetails.PhoneNumber,
                Message = message,
                UserId = userId
            };

            var messageContent = new StringContent(JsonConvert.SerializeObject(messageObject), System.Text.Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(uri, messageContent);
            response.EnsureSuccessStatusCode();
        }

        private string CreateInviteMessage(DebtorInviteSmsDTO details)
        {
            return $"Hi {details.DebtorName}, {details.BillOwnerName}'s added you to the bill: '{details.BillName}'! To pay: £{details.BillCost} a month - Use the code below " +
                $"as the reference for the payment to let them easily monitor the bills progress {details.DebtReference}";
        }
    }

    public class SmsMessageResponse
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public Guid UserId { get; set; }
    }
}
