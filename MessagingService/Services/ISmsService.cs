﻿using Messaging.API.Model;
using Messaging.API.Model.RequestModels;
using Messaging.API.Model.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Services
{
    public interface ISmsService
    {
        void AddSmsToDatabase(string phoneNumber, string message, Guid UserID);
        ICollection<SmsMessage> GetLatestSmsMessageBatch();
        void UpdateSmsDetails(SmsMessage smsMessage, ClickSendResponse messageDetails);
    }
}
