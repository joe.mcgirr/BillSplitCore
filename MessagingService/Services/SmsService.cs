﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messaging.API.Model;
using Messaging.API.Model.RequestModels;
using Messaging.API.Model.ResponseModels;
using Messaging.API.Repository;
using Microsoft.EntityFrameworkCore;

namespace Messaging.API.Services
{
    public class SmsService : ISmsService
    {
        ApplicationDbContext _context;
        public SmsService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void AddSmsToDatabase(string phoneNumber, string message, Guid userID)
        {
            var newMessage = new SmsMessage
            {
                UserId = userID,
                PhoneNumber = phoneNumber,
                Message = message,
                Created = DateTime.Now
            };
            _context.SmsMessages.Add(newMessage);
            _context.SaveChangesAsync();
        }

        public ICollection<SmsMessage> GetLatestSmsMessageBatch()
        {
            return _context.SmsMessages.Where(m => m.DateSent == default(DateTime)).OrderBy(m => m.Created).Take(20).ToList();
        }

        public void UpdateSmsDetails(SmsMessage smsMessage, ClickSendResponse messageDetails)
        {
            var pertainingSmS = _context.SmsMessages.Where(m => m.Id == smsMessage.Id).FirstOrDefault();
            if (pertainingSmS == null)
            {
                throw new Exception("A bill with the same ID as the one just sent doesn't exist in the database.");
            }
            pertainingSmS.DateSent = DateTime.Now;
            pertainingSmS.MessageParts = messageDetails.data.total_count;
            pertainingSmS.MessagePrice = messageDetails.data.total_price;
            _context.SaveChanges();
        }
    }
}
