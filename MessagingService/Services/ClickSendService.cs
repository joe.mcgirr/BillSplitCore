﻿using Messaging.API.Model;
using Messaging.API.Model.RequestModels;
using Messaging.API.Model.ResponseModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Messaging.API.Services
{
    public class ClickSendService : ISmsGateway
    {
        private readonly HttpClient _httpClient;

        public ClickSendService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<ClickSendResponse> SendMessageAsync(SmsMessage smsMessage)
        {
            var smsDTO = new SmsSendDTO
            {
                messages = new List<SendSmsMessageDTO>
                {
                    new SendSmsMessageDTO
                    {
                        source = "c#",
                        to = smsMessage.PhoneNumber, //"+61411111111",
                        body = smsMessage.Message,
                        from = "BillSplit",
                        schedule = "",
                        custom_string = "invite_message"
                    }
                }
            };
            
            var messageContent = new StringContent(JsonConvert.SerializeObject(smsDTO), System.Text.Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("sms/send", messageContent);
            //TODO: Add error handling for SMS gateway
            response.EnsureSuccessStatusCode();
            var jsonResponse = JsonConvert.DeserializeObject<ClickSendResponse>(response.Content.ReadAsStringAsync().Result);
            return jsonResponse;
            
        }
    }
}
