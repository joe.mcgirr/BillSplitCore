﻿using Messaging.API.Model;
using Messaging.API.Model.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Messaging.API.Services
{
    public interface ISmsGateway
    {
        Task<ClickSendResponse> SendMessageAsync(SmsMessage message);
    }
}
