﻿using Messaging.API.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Repository
{
    public class ApplicationDbContext : DbContext
    {
        protected readonly DbContextOptions _options;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
            _options = options;
        }

        public DbSet<SmsMessage> SmsMessages { get; set; }
    }
}
