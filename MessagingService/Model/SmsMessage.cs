﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model
{
    public class SmsMessage : Model
    {
        public Guid UserId { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateSent { get; set; }
        public int MessageParts { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MessagePrice { get; set; }
    }
}
