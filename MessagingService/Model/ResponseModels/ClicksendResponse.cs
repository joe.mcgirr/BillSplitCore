﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model.ResponseModels
{
    public class ClickSendResponse
    {
        public string response_msg { get; set; }
        public ResponseData data { get; set; }
    }
}
