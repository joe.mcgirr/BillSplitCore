﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model.ResponseModels
{
    public class ResponseData
    {
        public int total_count { get; set; }
        public decimal total_price { get; set; }
    }
}
