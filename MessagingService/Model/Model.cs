﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model
{
    public class Model
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
    }
}
