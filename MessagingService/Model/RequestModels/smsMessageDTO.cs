﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model.RequestModels
{
    public class SmsMessageDTO
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
        public Guid UserId { get; set; }
    }
}
