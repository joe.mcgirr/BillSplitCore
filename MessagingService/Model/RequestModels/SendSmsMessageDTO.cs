﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model.RequestModels
{
    public class SendSmsMessageDTO
    {
        //Lower case variables to match case-sensitive JSON body request
        public string source { get; set; }
        public string from { get; set; }
        public string body { get; set; }
        public string to { get; set; }
        public string schedule { get; set; }
        public string custom_string { get; set; }
    }
}
