﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messaging.API.Model.RequestModels
{
    public class SmsSendDTO
    {
        public List<SendSmsMessageDTO> messages { get; set; }
    }
}
