﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Messaging.API.Model.RequestModels;
using Messaging.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Messaging.API.Controllers
{
    [Route("api/SMS")]
    [ApiController]
    public class SmsController : Controller
    {
        ISmsService _smsService;
        public SmsController(ISmsService smsService)
        {
            _smsService = smsService;
        }

        [HttpPost("AddSmsToQueue")]
        public IActionResult AddSmsToQueue([FromBody]SmsMessageDTO smsMessage)
        {
            try
            {
                _smsService.AddSmsToDatabase(smsMessage.PhoneNumber, smsMessage.Message, smsMessage.UserId);
                return Ok(new
                {
                    Success = true
                });
            } catch(DbUpdateException e)
            {
                return BadRequest(new
                {
                    e.Message
                });
            }
        }
    }
}