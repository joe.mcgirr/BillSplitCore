﻿using Messaging.API.Model;
using Messaging.API.Services;
using Messaging.API.Tasks.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Messaging.API.Tasks
{
    public class Scheduler : IHostedService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private Timer _timer;

        public Scheduler(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(e => DoWork(cancellationToken), null, TimeSpan.Zero,
           TimeSpan.FromMinutes(1));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        private void DoWork(CancellationToken cancellationToken)
        {
            Console.WriteLine("Background task running");
            using (var scope = _scopeFactory.CreateScope())
            {
                var scheduledTasks = scope.ServiceProvider.GetServices<IScheduledTask>();

                foreach (var task in scheduledTasks)
                {
                    task.ExecuteAsync(cancellationToken);
                }
            }
        }
        
    }
}
