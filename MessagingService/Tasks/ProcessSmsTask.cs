﻿using Messaging.API.Model;
using Messaging.API.Services;
using Messaging.API.Tasks.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Messaging.API.Tasks
{
    public class ProcessSmsTask : IScheduledTask
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public ProcessSmsTask(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var smsGateway = scope.ServiceProvider.GetRequiredService<ISmsGateway>();
                var smsService = scope.ServiceProvider.GetRequiredService<ISmsService>();

                var smsBatch = smsService.GetLatestSmsMessageBatch();
                if(!cancellationToken.IsCancellationRequested)
                {
                    foreach (SmsMessage sms in smsBatch)
                    {
                        var response = await smsGateway.SendMessageAsync(sms);
                        try
                        {
                            smsService.UpdateSmsDetails(sms, response);
                            Console.WriteLine("Messages Updated");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }
    }
}
