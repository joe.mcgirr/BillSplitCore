﻿using System;
using Messaging.API.Repository;
using Messaging.API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Messaging.API.Tasks;
using Messaging.API.Tasks.Interfaces;

namespace MessagingService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add EF services to the services container.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<ISmsService, SmsService>();
            services.AddScoped<ISmsGateway, ClickSendService>();

            //scheduleder tasks
            services.AddSingleton<IScheduledTask, ProcessSmsTask>();

            //needs to be singleton
            services.AddHostedService<Scheduler>();

            var authorizationHeader = CreateClickSendAuthorizationHeader();
            services.AddHttpClient<ISmsGateway, ClickSendService>(
                client =>
                {
                    client.BaseAddress = new Uri(Configuration["ClickSendBaseUrl"]);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", authorizationHeader);
                })
                .SetHandlerLifetime(TimeSpan.FromMinutes(5));
                //Perform exponential backoff on failure
                //.AddPolicyHandler(GetRetryPolicy());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        private string CreateClickSendAuthorizationHeader()
        {
            var username = Configuration["ClickSendUserName"];
            var pass = Configuration["ClickSendKey"];
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(username + ":" + pass);
            var encodedString = Convert.ToBase64String(plainTextBytes);
            return encodedString;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ApplicationDbContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts(h => h.MaxAge(days: 365));
            }

            //app.UseHttpsRedirection();     

            app.UseMvc();
            dbContext.Database.EnsureCreated();
        }
    }
}
