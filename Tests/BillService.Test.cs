﻿using AutoMapper;
using BillSplit.AppStart;
using BillSplit.Models;
using BillSplit.Models.ResponseModels;
using BillSplit.Repository.Interfaces;
using BillSplit.Services;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Tests
{
    public class ServiceTests
    {
        [TestFixture]
        public class BillServiceTests
        {
            private readonly BillsService _billsService;
            private const int _monthSpread = 6;

            IBillsRepository _fakeBillRepo = A.Fake<IBillsRepository>();
            IMapper _fakeMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutomapperProfile());
            }).CreateMapper();
            IDebtorGroupRepo _fakeDebtorGroupRepo = A.Fake<IDebtorGroupRepo>();
            IDebtorRepository _fakeDebtorRepository = A.Fake<IDebtorRepository>();

            //6 month search span
            DateTime _searchStartDate = DateTime.Now.AddMonths(1 - (_monthSpread - 1));
            //search till month after current
            DateTime _searchEndDate = DateTime.Now.AddMonths(1);
            //bill guids
            Guid daily1Guid = Guid.NewGuid();

            decimal _dailyBillCost = 4.00M;
            decimal _weeklyBillCost = 8.00M;
            decimal _annualBillCost = 100M;

            public BillsService CreateBillService()
            {
                return new BillsService(_fakeBillRepo, _fakeMapper, _fakeDebtorGroupRepo,
                    A.Fake<IDebtorRepository>());
            }

            [Test]
            public void NewInstanceCreated()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);
            }

            private Debtor GetFirstUser(decimal billCost)
            {
                var debtorId = Guid.NewGuid();
                var debtor = new Debtor()
                {
                    Name = "Katie Botteril",
                    Id = debtorId,
                    PhoneNumber = "+61411111111",
                    DebtorGroups = new List<DebtorGroupInstance>()
                };

                var debtorGroup = new DebtorGroupInstance
                {
                    BillId = daily1Guid,
                    DebtorId = debtorId,
                    Debtor = debtor,
                    AddedToBill = DateTime.Now.AddMonths(-4),
                    Reminders = true,
                    DebtReference = "XYAKJFD",
                    AmountOwed = billCost,
                    DebtorPayments = new List<DebtorPayment>
                    {
                        new DebtorPayment
                        {
                            Date = _searchStartDate.AddMonths(3),
                            PaymentReference = "XYAKJFD",
                            AmountPayed = 4.00M,
                            TellerAccessId = Guid.NewGuid()
                        }
                    }
                };

                debtor.DebtorGroups.Add(debtorGroup);
                return debtor;
            }

            private List<Bill> GetCustomBillModelsPaidWithinDates(decimal billCost, Recurrence recurrence)
            {
                var bills = new List<Bill>()
                {
                    new Bill
                    {
                        Id = daily1Guid,
                        Name = "DailyBill1",
                        Cost = billCost,
                        StatementReference = "XYAKJFD",
                        Recurrence = recurrence,
                        FirstPayment = _searchStartDate,
                        LinkedToBank = true,
                        Payments = new List<BillPayment>
                        {
                            new BillPayment()
                            {
                                BillId = daily1Guid,
                                AmountPayed = 4.00M,
                                Date = _searchStartDate.AddMonths(1),
                                Reference = "XYAKJFD",
                                TellerAccessId = Guid.NewGuid()
                            }
                        },
                        DebtorGroups = GetFirstUser(billCost).DebtorGroups
                    }
                };

                return bills;
            }


            //TODO: REWRITE this test so it is self-contained and works with any data
            [Test]
            public void GetBillOverview_DailyBill_NoLastPayment_BillCostIsDaysOfMonthTimesCost()
            {
                var svc = CreateBillService();

                A.CallTo(() => _fakeBillRepo.GetBillsAndDebtorsWithPayments(A<DateTime>.Ignored,
                                        A<DateTime>.Ignored, A<Expression<Func<Bill, bool>>>.Ignored,
                                        null, null)).Returns(GetCustomBillModelsPaidWithinDates(_dailyBillCost, Recurrence.daily));

                //current userId doesn't matter as getBillsAndSebtorsWithPayments is faked
                var result = svc.GetBillOverviews(_searchStartDate, _searchEndDate, Guid.NewGuid());
                int totalDaysInDateRange = 0;
                for (var i = 0; i < _monthSpread; i++)
                {
                    //need to use the first day incase testing on the 29th of a month..
                    var firstDayOfMonthInRange = new DateTime(_searchStartDate.Year, _searchStartDate.Month, 1).AddMonths(i);
                    totalDaysInDateRange = DateTime.DaysInMonth(firstDayOfMonthInRange.Year, firstDayOfMonthInRange.Month);
                    var monthlyBillCost = result[i].DebtorMonthlyOverviews[0].TotalOwed;
                    Assert.AreEqual(monthlyBillCost, totalDaysInDateRange * _dailyBillCost);
                }

            }
            [Test]
            public void GetBillOverview_DailyBill_LastPaymentBeforeStartDate_BillNotReturned()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);
            }
            [Test]
            public void GetBillOverview_DailyBill_LastPaymentAfterEndDate_BillCostIsDaysOfMonthTimesCost()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);
            }
            [Test]
            public void GetBillOverview_DailyBill_lastPaymentHalfWayThroughEndDate_LastMonthCostIsCostMinusRemianingDays()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);

                var fakeBills = GetCustomBillModelsPaidWithinDates(_dailyBillCost, Recurrence.daily);
                var firstDayInSearch = new DateTime(_searchEndDate.Year, _searchEndDate.Month, 1);
                var lastDayInSearchMonth = firstDayInSearch.AddMonths(1).AddDays(-1);
                //bills last payment is 15 days from the end of the month
                fakeBills[0].LastPayment = lastDayInSearchMonth.AddDays(-15);

                A.CallTo(() => _fakeBillRepo.GetBillsAndDebtorsWithPayments(A<DateTime>.Ignored,
                                        A<DateTime>.Ignored, A<Expression<Func<Bill, bool>>>.Ignored,
                                        null, null)).Returns(fakeBills);

                //current userId doesn't matter as getBillsAndSebtorsWithPayments is faked         
                var result = svc.GetBillOverviews(_searchStartDate, _searchEndDate, Guid.NewGuid());
                var actualLastMonthsCost = result[_monthSpread - 1].DebtorMonthlyOverviews[0].TotalOwed;

                var expectedLastMonthCostMinusWeeks = (lastDayInSearchMonth.AddDays(-15).Day) * _dailyBillCost;

                Assert.AreEqual(expectedLastMonthCostMinusWeeks, actualLastMonthsCost);
            }

            [Test]
            public void GetBillOverview_DailyBill_LastPaymentHalfwayThroughSearchDates_RemainingMonthsHaveNoBill()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);

                var fakeBills = GetCustomBillModelsPaidWithinDates(_dailyBillCost, Recurrence.daily);
                var firstDayInBillEnd = new DateTime(_searchEndDate.Year, _searchEndDate.Month, 1);
                var lastDayInSearchMonth = firstDayInBillEnd.AddMonths(1).AddDays(-1);
                var halfwayThroughSearch = -(_monthSpread / 2);
                //last payment halfway through search spread - last day of month halfway through
                var dateOfBillEnd = firstDayInBillEnd.AddMonths(halfwayThroughSearch + 1).AddDays(-1);
                fakeBills[0].LastPayment = dateOfBillEnd;

                A.CallTo(() => _fakeBillRepo.GetBillsAndDebtorsWithPayments(A<DateTime>.Ignored,
                                        A<DateTime>.Ignored, A<Expression<Func<Bill, bool>>>.Ignored,
                                        null, null)).Returns(fakeBills);

                //current userId doesn't matter as getBillsAndSebtorsWithPayments is faked         
                var result = svc.GetBillOverviews(_searchStartDate, _searchEndDate, Guid.NewGuid());
                var firstDayOfStartSearchDates = new DateTime(_searchStartDate.Year, _searchStartDate.Month, 1);

                for (var i = 0; i < _monthSpread; i++)
                {
                    var currentMonth = firstDayOfStartSearchDates.AddMonths(i);
                    var daysInCurrentMonth = DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month);
                    if (i >= _monthSpread / 2)
                    {
                        Assert.IsTrue(result[i].DebtorMonthlyOverviews.Count == 0);
                    }
                    else
                    {
                        Assert.AreEqual(result[i].DebtorMonthlyOverviews[0].TotalOwed, daysInCurrentMonth * _dailyBillCost);
                    }
                }
            }

            [Test]
            public void GetBillOverview_DailyBill_FirstPaymentEqualToEndSearchDate_BillIsVisibleInLastMonth()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);
            }
            [Test]
            public void GetBillOverview_DailyBill_LastPaymentEqualToStartSearchDate_BillVisibleInFirstMonth()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);
            }

            [Test]
            public void GetBillOverview_WeeklyBill_lastPaymentHalfWayThroughEndDate_CostIsCostMinusRemianingDays()
            {
                var svc = CreateBillService();
                Assert.IsInstanceOf<BillsService>(svc);

                var fakeBills = GetCustomBillModelsPaidWithinDates(_weeklyBillCost, Recurrence.weekly);
                var firstDayInSearch = new DateTime(_searchEndDate.Year, _searchEndDate.Month, 1);
                var lastDayInSearchMonth = firstDayInSearch.AddMonths(1).AddDays(-1);
                //bills last payment is 15 days from the end of the month
                fakeBills[0].LastPayment = lastDayInSearchMonth.AddDays(-15);

                A.CallTo(() => _fakeBillRepo.GetBillsAndDebtorsWithPayments(A<DateTime>.Ignored,
                                        A<DateTime>.Ignored, A<Expression<Func<Bill, bool>>>.Ignored,
                                        null, null)).Returns(fakeBills);

                //current userId doesn't matter as getBillsAndSebtorsWithPayments is faked

                var result = svc.GetBillOverviews(_searchStartDate, _searchEndDate, Guid.NewGuid());
                var actualLastMonthsCost = result[_monthSpread - 1].DebtorMonthlyOverviews[0].TotalOwed;

                var dayBillFinishes = (lastDayInSearchMonth.AddDays(-15).Day);
                var weekBillFinishes = Convert.ToInt64(Math.Floor(Convert.ToDouble(dayBillFinishes / 7)));
                var expectedLastMonthCostMinusWeeks = weekBillFinishes * _weeklyBillCost;

                Assert.AreEqual(expectedLastMonthCostMinusWeeks, actualLastMonthsCost);
            }

            [Test]
            public void GetBillOverview_AnnualBill_BillIsDueWithinSearchDates_BillExistsWithinMonths()
            {
                var svc = CreateBillService();
                var fakeBills = GetCustomBillModelsPaidWithinDates(_annualBillCost, Recurrence.yearly);
                var firstDayInSearch = new DateTime(_searchEndDate.Year, _searchEndDate.Month, 1);
                var lastDayInSearchMonth = firstDayInSearch.AddMonths(1).AddDays(-1);
                //first payment made a year ago from the end of search date
                fakeBills[0].FirstPayment = lastDayInSearchMonth.AddYears(-1);
                A.CallTo(() => _fakeBillRepo.GetBillsAndDebtorsWithPayments(A<DateTime>.Ignored,
                                        A<DateTime>.Ignored, A<Expression<Func<Bill, bool>>>.Ignored,
                                        null, null)).Returns(fakeBills);

                //current userId doesn't matter as getBillsAndSebtorsWithPayments is faked
                var result = svc.GetBillOverviews(_searchStartDate, _searchEndDate, Guid.NewGuid());
                var lastMonthsCost = result[_monthSpread - 1].DebtorMonthlyOverviews[0].TotalOwed;
                var secondButLastMonthOverview = result[_monthSpread - 2].DebtorMonthlyOverviews.Count;
                Assert.AreEqual(lastMonthsCost, _annualBillCost);
                Assert.AreEqual(0, secondButLastMonthOverview );
            }
        }
    }
}
